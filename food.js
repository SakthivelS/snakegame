let food = randomGridPosition();
import { onSnake, expandSnake } from "./snake.js";

export function update() {
    //if snake came on food then change the food position and expand snake
    if (onSnake(food)) {
        expandSnake();
        food = randomGridPosition();
    }
}
export function draw(gameBoard) {
    let foodElement = document.createElement("div");
    foodElement.style.gridRowStart = food.y;
    foodElement.style.gridColumnStart = food.x;
    foodElement.classList.add("food");
    gameBoard.appendChild(foodElement);
}

function randomFoodPosition() {
    let newFoodPosition;
    //create a newfood except on snake position and ramdom position
    while (newFoodPosition == null || onSnake(newFoodPosition)) {
        newFoodPosition = randomGridPosition();
    }
    return newFoodPosition;
}

function randomGridPosition() {
    //get a random value betwen 1-21
    return {
        x: Math.floor(Math.random() * 21) + 1,
        y: Math.floor(Math.random() * 21) + 1,
    };
}
