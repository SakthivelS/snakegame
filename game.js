let lastRenderTime = 0;
let gameBoard = document.getElementById("game-board");
let gameOver = false;
let endGame = new Audio("./endGame.mp3");
import {
    snakeSpeed,
    update as updateSnake,
    draw as drawSnake,
    snakeHead,
    snakeIntersection,
} from "./snake.js";
import { update as updateFood, draw as drawFood } from "./food.js";

function main(currentTime) {
    //Check if the game is over
    if (gameOver) {
        endGame.play();
        if (confirm("You lost!!!! Wanna play again?")) {
            window.location = "/";
        }
        return;
    }
    //Call the funtion in loop
    window.requestAnimationFrame(main);
    let secondsSinceLastRender = (currentTime - lastRenderTime) / 1000;
    //refresh the frames as per the snakespeed that we have.. if the seconds hasnt reached snake speed, just return without udpdating
    if (secondsSinceLastRender < 1 / snakeSpeed) return;
    lastRenderTime = currentTime;
    //console.log("render");
    update();
    draw();
    document.getElementsByClassName(
        "highScore"
    )[0].innerHTML = localStorage.getItem("highScore");
}

//call the funtion for first time
//requestAnimationFrame is executed whenever can give a new frame
window.requestAnimationFrame(main);

function update() {
    updateSnake();
    updateFood();
    checkDeath();
}

function draw() {
    gameBoard.innerHTML = "";
    drawSnake(gameBoard);
    drawFood(gameBoard);
}

function checkDeath() {
    gameOver = outsideGrid(snakeHead()) || snakeIntersection();
}

function outsideGrid(position) {
    //since our grid is 1-21 , if the position is outside, the return true
    return (
        position.x < 1 || position.x > 21 || position.y < 1 || position.y > 21
    );
}
