import { getInputDirection } from "./input.js";
let score = 0;
let eat = new Audio("./eat.mp3");

//Default speed will be 2 which 0.5 seconds
export let snakeSpeed = 2;
let snakeBody = [{ x: 11, y: 11 }];
let newSegments = 0;

export function update() {
    addSegments();
    //get direction from the "input" file
    const direction = getInputDirection();
    //increase the snake by 1 cell
    for (let i = snakeBody.length - 2; i >= 0; i--) {
        snakeBody[i + 1] = { ...snakeBody[i] };
    }
    //increase the snake in the direction of key press
    snakeBody[0].x += direction.x;
    snakeBody[0].y += direction.y;
    //Increase the speed by 1 for every 10 increase in score
    snakeSpeed = Math.floor(score / 10 + 2);
}
export function draw(gameBoard) {
    // draw the snake for every position in the grid
    snakeBody.forEach((segment) => {
        let snakeElement = document.createElement("div");
        snakeElement.style.gridRowStart = segment.y;
        snakeElement.style.gridColumnStart = segment.x;
        snakeElement.classList.add("snake");
        gameBoard.appendChild(snakeElement);
    });
}

export function onSnake(position, { ignoreHead = false } = {}) {
    // check if any x& y of snake match with x&y of the "position"
    //ignoreHead is by default false. It will be called with true when check snake head is on snake
    return snakeBody.some((segment, index) => {
        if (ignoreHead && index == 0) return false;
        return segment.x === position.x && segment.y === position.y;
    });
}

export function expandSnake() {
    //add one more segment to snake
    newSegments += 1;

    //increase the score and play the eatting sound
    score += 1;
    eat.play();
    document.getElementsByClassName("score")[0].innerHTML = score;
    // If score is greater than
    if (score > localStorage.getItem("highScore"))
        localStorage.setItem("highScore", score);
}

function addSegments() {
    //add segments will add the newsegments to the snakebody
    for (let i = 0; i < newSegments; i++) {
        snakeBody.push({ ...snakeBody[snakeBody.length - 1] });
    }
    newSegments = 0;
}

export function snakeHead() {
    //First element of the snake body will the head
    return snakeBody[0];
}

export function snakeIntersection() {
    //check if the head is on any poisition of snake except the head itself(thats why we pass ignoreHead:true)
    return onSnake(snakeBody[0], { ignoreHead: true });
}
